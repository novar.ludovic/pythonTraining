import os
from Personne import *
from Compteur import *
from TableauNoir import *
from ZDict import *
from Duree import *
from Temp import *
from Etudiant import *
from LigneInventaire import *
from operator import attrgetter
from MonException import *

from DicOrdonne import *


print(os.getcwd())

dico = ZDict()
jean = Personne()
duree = Duree(3,50)

print(jean.nom)
print(jean.prenom)

a = Compteur()
print(Compteur.objets_crees)
b = Compteur()
print(Compteur.objets_crees)

print(jean.__dict__)
print(jean.lieu_residence)

print(jean)

print(jean.e)

jean.prenom = "Ludovic"

print(jean.e)

dico['Toast'] = 'toto'
print(dico['Toast'])

print('Toast' in dico)

print(len(dico))

print(duree)

print(15+duree)

print(duree)

duree+=15

print(duree)

print(dict(duree.__dict__))

del jean

#_____________________________________________________________________

print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Tri de liste $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')

etudiants = [
    Etudiant("Clément", 14, 16),
    Etudiant("Charles", 12, 15),
    Etudiant("Oriane", 14, 18),
    Etudiant("Thomas", 11, 12),
    Etudiant("Damien", 12, 15),
]

print(etudiants)

print(sorted(etudiants,key=Etudiant.moyenneSortKey))

#print(sorted(etudiants, key=lambda etudiant: etudiant[2]))

print(sorted(etudiants,key=attrgetter("moyenne")))
print(sorted(etudiants,key=attrgetter('age',"moyenne")))

inventaire = [
    LigneInventaire("pomme rouge", 1.2, 19),
    LigneInventaire("orange", 1.4, 24),
    LigneInventaire("banane", 0.9, 21),
    LigneInventaire("poire", 1.2, 24),
]

print(inventaire)

print(sorted(inventaire, key=attrgetter("prix", "quantite")))


def intervalle(borne_inf, borne_sup):
    """Générateur parcourant la série des entiers entre borne_inf et borne_sup.

    Note: borne_inf doit être inférieure à borne_sup"""

    if borne_inf<borne_sup:
        while borne_inf <= borne_sup:
            yield borne_inf
            borne_inf += 1
    else:
        print('Toast')
        while borne_inf>=borne_sup:
            yield borne_inf
            borne_inf-=1


def intervalleV2(borne_inf, borne_sup):
    """Générateur parcourant la série des entiers entre borne_inf et borne_sup.
    Notre générateur doit pouvoir "sauter" une certaine plage de nombres
    en fonction d'une valeur qu'on lui donne pendant le parcours. La
    valeur qu'on lui passe est la nouvelle valeur de borne_inf.

    Note: borne_inf doit être inférieure à borne_sup"""
    borne_inf += 1
    while borne_inf < borne_sup:
        valeur_recue = (yield borne_inf)
        if valeur_recue is not None:  # Notre générateur a reçu quelque chose
            borne_inf = valeur_recue
            #yield borne_inf
        borne_inf += 1

for number in iter(intervalle(5,10)):
    print(number)
for number in iter(intervalle(10,5)):
    print(number)


gen = intervalle(5,20)
for number in gen:
    print(number)
    if number > 17:
        gen.close()

generateur = intervalleV2(10, 25)
for nombre in generateur:
    if nombre == 15:  # On saute à 20
        generateur.send(20)
    print(nombre, end=" ")


#raise MonException("Balingue bing")

#help(TableauNoir)

dicor1 = DicOrdonne(Toto=1,Titi=2)


#dicor2 = DicOrdonne(keys=akeys,values=avalues)

print()


print(dicor1)
#print(dicor2)

