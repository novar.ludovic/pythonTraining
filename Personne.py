class Personne:
    """Classe définissant une personne caractérisée par :
    - son nom
    - son prénom
    - son âge
    - son lieu de résidence"""

    def __init__(self,nom=None,prenom=None):
        self.nom = nom if None else "Dupont"
        self.prenom = prenom if None else "Jean"  # Quelle originalité
        self.age = 33  # Cela n'engage à rien
        self._lieu_residence = "Paris"  # Notez le souligné _ devant le nom

    def __str__(self):
        """Méthode permettant d'afficher plus joliment notre objet"""
        return "{} {}, âgé de {} ans".format(
                self.prenom, self.nom, self.age)

    def __repr__(self):
        """Quand on entre notre objet dans l'interpréteur"""
        return "Personne: nom({}), prénom({}), âge({})".format(
            self.nom, self.prenom, self.age)

    def __del__(self):
        print("C'est la fin")

    def __getattr__(self, nom):
        """Si Python ne trouve pas l'attribut nommé nom, il appelle
            cette méthode. On affiche une alerte"""
        print("Alerte ! Il n'y a pas d'attribut {} ici !".format(nom))
        return self.prenom

    def __setattr__(self, nom_attr, val_attr):
        """Méthode appelée quand on fait objet.nom_attr = val_attr.
        On se charge d'enregistrer l'objet"""

        object.__setattr__(self, nom_attr, val_attr)
        self.enregistrer()

    def __delattr__(self, nom_attr):
        """On ne peut supprimer d'attribut, on lève l'exception
        AttributeError"""

        raise AttributeError("Vous ne pouvez supprimer aucun attribut de cette classe")

    def _get_lieu_residence(self):
        """Méthode qui sera appelée quand on souhaitera accéder en lecture
        à l'attribut 'lieu_residence'"""
        print("On accède à l'attribut lieu_residence !")
        return self._lieu_residence

    def _set_lieu_residence(self, nouvelle_residence):
        """Méthode appelée quand on souhaite modifier le lieu de résidence"""
        print("Attention, il semble que {} déménage à {}.".format(self.prenom, nouvelle_residence))
        self._lieu_residence = nouvelle_residence

    def enregistrer(self):
        print("L'objet à été enregistrer en BDD")

    # On va dire à Python que notre attribut lieu_residence pointe vers une
    # propriété
    lieu_residence = property(_get_lieu_residence, _set_lieu_residence)
