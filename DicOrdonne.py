class DicOrdonne:
    def __init__(self,dico=None,**entries):
        self._dictionnaire = dict(entries)
        if dico is not None:
            if isinstance(dico,dict):
                self._dictionnaire.update(dico)
            else:
                raise TypeError("Ca n'est pas un dictionnaire")
    def __str__(self):
        return self._dictionnaire.__str__()

# class DicOrdonne:
#
#     def __init__(self,keys=None,values=None,dict=None):
#         self.keys=[]
#         self.values=[]
#         if keys is not None and values is not None:
#             print("Toast")
#             self.keys = keys
#             self.values = values
#         if dict is not None:
#             for key in dict.keys():
#                 self.keys.append(key)
#             for value in dict.values():
#                 self.values.append(value)
